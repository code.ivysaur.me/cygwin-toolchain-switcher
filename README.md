# cygwin-toolchain-switcher

![](https://img.shields.io/badge/written%20in-Bash-blue)

Switch 'gcc' in Cygwin to point to another toolchain.

This script configures symlinks in `$PATH` so that invoking e.g. `gcc` or `ld` will point to a specific toolchain instead of the native one.

## Usage

Running `cygwin-toolchain-switcher.sh` will create and set up toolchain switching scripts. Once the script has run, you can switch toolchains at any time by typing:

`. ~/.cygwin-toolchain-switcher/use-toolchain-x86_64-mingw-w64`

There are scripts installed for each detected toolchain.

## Notes

If you want to have `gcc` invocations work all the way through Cygwin -> Windows -> Cygwin roundtrips (e.g. using cygwin `git` with windows `go get` with cygwin `x86_64-mingw-w64-gcc` to make `CGO` work) then the created symlinks must be Windows symlinks instead of Cygwin symlinks. To do so, launch Cygwin as Administrator and `export CYGWIN="winsymlinks:nativestrict"` before invoking the script.


## Download

- [⬇️ cygwin-toolchain-switcher-v1.tar.gz](dist-archive/cygwin-toolchain-switcher-v1.tar.gz) *(738B)*
